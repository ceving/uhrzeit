# Uhrzeit-Lern-App für Eltern und Kinder

## Beschreibung
Die App ist ein Hilfmittel für Eltern, mit dem sie ihren Kindern die Uhrzeit
einfach erklären können.

## Funktionen
Es können die verschiedenen Elemente einer Uhr gezielt ein- und ausgeblendet
werden. Kinder können selber an den Zeigern drehen und so den Zusammenhang
zwischen Stunden- und Minuten-Skala unmittelbar begreifen. Durch die
Kombination von Analog-Uhr und Digital-Uhr kann die eingestellte Zeit an
der Analog-Uhr direkt kontrolliert werden. Durch die Möglichkeit, die 
Digital-Uhr auszublenden, können Übungen durchgeführt werden, die dem Kind
sofort zeigen, ob die Zeit an der Analog-Uhr richtig eingestellt wurde.

## Online-Version
Die Online-Version ist unter der URL

* http://ceving.gitlab.io/uhrzeit/

abrufbar.

## Systemanforderungen
Das Programm erfordert einen aktuellen HTML 5 Browser mit SVG-Support.
Ich habe mit Firefox-Quantum entwickelt und getestet.

## Lizenz
Das Programm kann gemäß der Bestimmungen der "GNU Affero General Public"-Lizenz
in der Version 3 verwendet werden.